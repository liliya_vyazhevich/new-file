const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const concat = require('gulp-concat');
const browserSync = require('browser-sync');
const pug = require('gulp-pug');

gulp.task('pug',function(){
    gulp.src ('./src/pages/**/*.pug')
    .pipe(pug({
        pretty: true
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('sass',function () {
    return gulp.src ('./src/**/*.scss')
    .pipe(sass().on('error',sass.logError))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./dist/styles'));
});

gulp.task('reloader',function() {
    browserSync({
        server:{
            baseDir: './dist/'
        }
    });
});

gulp.task ('image',function () {
    return gulp.src ('./src/assets/image/*.+jpeg|png|svg')
    pipe(image())
    pipe(gulp.dest('./dist/assets'));
});

gulp.task('watch',['sass','reloader','pug'],function(){
    gulp.watch('./dist/**/*.html',browserSync.reload);
    gulp.watch('./src/**/*.scss',['sass']);
    gulp.watch('./dist/styles/**/*.css').on('change',browserSync.reload);
    gulp.watch('./src/pages/**/*.pug',['pug']);
});